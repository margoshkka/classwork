import multiprocessing


def test(num):
    print('worker:', num)


if __name__ == '__main__':
    processes = []
    for i in range(5):
        processes.append(multiprocessing.Process(target=test, args=(i,)))
    for pr in processes:
        pr.start()
