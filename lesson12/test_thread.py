import threading


def test(num):
    print('worker:', num)


if __name__ == '__main__':
    processes = []
    for i in range(5):
        processes.append(threading.Thread(target=test, args=(i,)))
    for pr in processes:
        pr.start()
