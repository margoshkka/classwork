def lazy_range(up_to):
    index = 0
    while index < up_to:
        jump = yield index
        if not jump:
            jump = 1
        index += jump


if __name__ == '__main__':
    gen = lazy_range(5)
    print(gen)
    print(next(gen))
    print(next(gen))
    print(gen.send(3))
