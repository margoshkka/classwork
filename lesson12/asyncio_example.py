import asyncio


async def countdown(number, n):
    while n > 0:
        print('%s minus %s' % (n, number))

        await asyncio.sleep(0)
        n-=number


loop = asyncio.get_event_loop()
tasks = [
    asyncio.ensure_future(countdown(1,15)),
    asyncio.ensure_future(countdown(2,14))
]
loop.run_until_complete(asyncio.wait(tasks))
