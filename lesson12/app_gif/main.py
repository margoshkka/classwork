from aiohttp import web
import asyncio_redis
from lesson12.app_gif.routes import setup_routes
from lesson12.app_gif.settings import config
import aioredis


async def init_db(app):
    conf = app['config']['redis']
    # connection = await asyncio_redis.Connection.create(host=conf['host'], port=conf['port'])
    connection = await aioredis.create_connection(address=(conf['host'],conf['port']))
    app['conn'] = connection


app = web.Application()
setup_routes(app)
app['config']=config
app.on_startup.append(init_db)
web.run_app(app,host='127.0.0.1',port=5050) # can add host and port
