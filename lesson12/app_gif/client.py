from aiohttp import ClientSession
from  asyncio import *


async def ws_client():
    session = ClientSession()
    async with session.ws_connect('http://127.0.0.1:5050/ws') as ws:
        await prompt(ws)
        async for msg in ws:
            print('Receive from server', msg.data)
            await prompt(ws)


async def prompt(ws):
    msg = input('type message:')
    await ws.send_str(msg)


get_event_loop().run_until_complete(ws_client())