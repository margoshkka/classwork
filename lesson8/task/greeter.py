# Напишите класс Greeter
from datetime import datetime
import logging
import sys

class Greeter:
    # с функцией greet которая получает имя на входе и возвращает строку “Привет <name>”.
    # Параметры метода не должны меняться на протяжении всего упражнения.
    # Структуру класса Greeter вы можете задать по своему усмотрению.
    @staticmethod
    def greet(name):
        stripped = name.strip()
        capitalized = stripped.capitalize()

        if datetime.now().hour >= 6 and datetime.now().hour <= 12:
            result = "Good morning " + capitalized
        elif datetime.now().hour >= 18 and datetime.now().hour <= 22:
            result = "Good evening " + capitalized
        elif datetime.now().hour in range(22,24) or datetime.now().hour in range(0,6):
            result = "Good night " + capitalized
        else:
            result = "Hello " + capitalized

        logging.info(result)
        return result

