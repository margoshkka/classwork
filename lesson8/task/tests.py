from unittest import TestCase, mock
from lesson8.task.greeter import Greeter


class GreeterTestCase(TestCase):
    @mock.patch('lesson8.task.greeter.datetime')
    def test_greet_say_hello(self, mocked_datetime):
        mocked_datetime.now.return_value.hour = 15
        test_name = 'Margo'
        result = Greeter.greet(test_name)
        self.assertEqual(result, "Hello " + test_name)

    def test_greet_stripped_name(self):
        param = " Str "
        result = Greeter.greet(param)
        word_list = result.split()
        self.assertEqual(word_list[-1], param.strip())

    def test_greet_capitalize_name(self):
        name = "jane"
        result = Greeter.greet(name)
        word_list = result.split()
        self.assertEqual(word_list[-1], name.capitalize())

    # greet возвращает “Доброе утро <name>”
    #  когда текущее время 06:00-lesson12:00
    @mock.patch('lesson8.task.greeter.datetime')
    def test_greed_good_morning(self, mocked_datetime):
        mocked_datetime.now.return_value.hour = 8
        test_name = 'Mike'
        result = Greeter.greet(test_name)
        self.assertEqual(result, "Good morning " + test_name)

    # greet возвращает “Добрый вечер <name>” когда текущее время 18:00-22:00
    @mock.patch('lesson8.task.greeter.datetime')
    def test_greed_good_evening(self, mocked_datetime):
        mocked_datetime.now.return_value.hour = 20
        test_name = 'Jane'
        result = Greeter.greet(test_name)
        self.assertEqual(result, "Good evening " + test_name)

    # greet возвращает “Доброй ночи <name>” когда текущее время 22:00-06:00
    @mock.patch('lesson8.task.greeter.datetime')
    def test_greed_good_night(self, mocked_datetime):
        mocked_datetime.now.return_value.hour = 23
        test_name = 'Andy'
        result = Greeter.greet(test_name)
        self.assertEqual(result, "Good night " + test_name)

    # greet пишет лог каждый раз когда вызывается
    @mock.patch('lesson8.task.greeter.logging')
    def test_check_logs(self, mock_logging):
        # mock_logging.info.assert_called_with(Greeter.greet("name"))
        self.assertLogs(mock_logging, mock_logging.info)



