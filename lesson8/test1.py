from unittest import mock, TestCase
from _md5 import md5


class StringTestCase(TestCase):
    def test_upper(self):
        self.assertEqual('foo'.upper(), 'Foo')

    # def setUp(self):

    @mock.patch('hashlib.md5')
    def test_profile(self, mock_md5):
        # profile = Mock(first_name='Ivan')
        # print(profile.first_name)
        # profile.balance.return_value = 15
        # print(profile.balance)
        # profile.balance.side_effect = Exception('Some error')
        # profile.balance() # return exception
        mock_md5.return_values = 'Some hash'
        mock_md5.assert_called_with('some param')
        mock_md5.calls


def hash_md5():
    return md5('SomeStr').excode()

