from unittest import TestCase, mock
from lesson8.hash import hash_md5


class MD5TestCase(TestCase):
    @mock.patch('lesson8.hash.md5')
    def test_some_test(self, mock_md5):
        hash_md5('Some Str')
        mock_md5.assert_called_with("Some Str".encode())

    mock.Mock()
