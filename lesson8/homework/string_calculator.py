class StringCalculator:
    @staticmethod
    def add(input_string):
        values =[]
        # when empty
        if not input_string:
            return 0

        # when the only one
        if len(input_string) == 1:
            return int(input_string)

        # transform all separators into ' '
        if input_string.startswith('//'):
            input_string = input_string.replace('//','')
            input_string = input_string.replace('###','#')
            input_string = input_string.replace('#', ' ')

        input_string = input_string.replace(',', ' ')

        # remove ' ' and \n
        arr = input_string.split()

        # convert all string values to integers inside of array
        values.extend([int(i) for i in arr])

        # check if we have negative numbers
        if len([i for i in values if i < 0]) > 0:
            raise Exception('Negative values are not allowed.')

        # ignore all numbers > 1000
        [values.remove(i) for i in values if i > 1000]

        # get sum
        result = sum(values)
        return result


if __name__ == "__main__":
    StringCalculator.add("1,2\n55")