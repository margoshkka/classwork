from unittest import TestCase, mock
from lesson8.homework.string_calculator import StringCalculator


class StringCalculatorTestCase(TestCase):
    # Пустая строка возвращает ноль ('' => 0)
    def test_empty_string(self):
        result = StringCalculator.add("")
        self.assertEqual(result, 0)

    # Единственное число возвращает значение этого числа ('1' => 1 '2' => 2)
    def test_one_number(self):
        result = StringCalculator.add("8")
        self.assertEqual(result, 8)

    # Два числа разделенные запятой возвращают сумму ('1,2' => 3 '10,20' => 30)
    def test_two_numbers_separated_by_comma(self):
        result = StringCalculator.add("1,2")
        self.assertEqual(result, 3)

    # Два числа разделенные переходом строки возвращают сумму ('1\n2' => 3)
    def test_two_numbers_separated_by_endl(self):
        result = StringCalculator.add("1\n2")
        self.assertEqual(result, 3)

    # Три числа разделенные запятой или переходом строки возвращают сумму ('1\n2,3\n4' => 10)
    def test_three_numbers_sep_ny_comma_or_endl(self):
        result = StringCalculator.add("1\n2,3\n4")
        self.assertEqual(result, 10)

    # Отрицательные числа возвращают ошибку с сообщением ('-1,2,-3' => 'отрицательные числа запрещены: -1,-3')
    def test_negative_numbers(self):
        with self.assertRaises(Exception) as ex:
            StringCalculator.add("-1,2,-3")
            self.assertEqual(ex.msg, 'Negative values are not allowed.')

    # Числа больше чем 1000 игнорируются
    def test_big_values(self):
        result = StringCalculator.add("1 1001 15")
        self.assertEqual(result, 16)

    # Разделитель из одного знака может быть задан на первой
    # строке начиная с // (к примеру //#\n1#2 для того чтобы
    #  задать ‘#’ как разделитель)
    def test_single_separator(self):
        result = StringCalculator.add("//#\n1#2")
        self.assertEqual(result, 3)

    # Разделитель из нескольких знаков может быть задан на первой строке начиная //
    # (например //###\n1###2 для того чтобы задать ‘###’ как разделитель)
    def test_multiple_separator(self):
        result = StringCalculator.add("//###\n1###2")
        self.assertEqual(result, 3)
