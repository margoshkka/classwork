#
# x = ( i*2 for i in range(10) if i % 2 == 0)
# print('\n' + str(x))

def Foo():
    print('pre')
    yield 1
    print('in')
    yield 2
    print('post')


x = (i*2 for i in range(10))
y = Foo()
it = iter(y)

try:
    next(it)
    next(it)
    next(it)
    next(it)
except StopIteration:
    print(StopIteration.args)