class Foo:
    def __init__(self):
        self.i = 0
        self.x = [1, 2, 3]

    def __iter__(self):
        return self

    def __next__(self):
        pass
        try:
            return self.x[self.i]

        except IndexError:
            raise StopIteration

        finally:
            self.i = self.i + 1


f = Foo()

for i in f:
    print(i)

for i in f:
    print(i)
