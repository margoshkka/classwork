from celery import Celery

app = Celery('tasks', broker='redis://localhost',backend='redis://localhost')


@app.task
def add(x,y):
    return x+y


# celery -A lesson15.tasks worker --loglevel=info
# add.delay(2,2)
# add.apply_async([2],{'y':2},countdown=5)
# delay in seconds when we need to do task - countdown=5

# result.ready()
# result.successful()
# result.failed()





