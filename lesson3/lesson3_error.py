# exceptions in python


class AnimalError(Exception):
    def __init__(self, animal):
        super().__init__()
        self.animal = animal

    def __str__(self):
        return "error: %s" % str(self.animal)




