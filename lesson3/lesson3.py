# OOP in Python
# decorators https://habr.com/post/141411/

# from lesson3.lesson3_error import AnimalError


class AnimalError(Exception):
    def __init__(self, animal):
        super().__init__()
        self.animal = animal

    def __str__(self):
        return "error: %s" % str(self.animal)


def butter(fn):
    def wrapper(self):
        print("see butter")
        fn(self)

    return wrapper


class Animal:
    def say(self):
        raise NotImplementedError

    def __init__(self, name):
        self.name = name


class Cat(Animal):
    @butter
    def say(self, msg):
        print(self.name + ' says:')
        print('meow')

        if msg == "":
            raise AnimalError(self)

    def __init__(self, name, color):
        super().__init__(name)
        self.color = color


# create cats
cat_alice = Cat('Alice', 'brown')
cat_murchik = Cat('Murchik', 'White')
cat_vasya = Cat('Vasya', 'Black')
cat_kuzya = Cat('Kuzya', 'White')


# alice info
print('\nTell me about your pet \n')
print('Which is a cat: ' + cat_alice.name)
print('which is cat`s Color: ' + cat_alice.color + '\n')

try:
    cat_alice.say("")
except AnimalError as e:
    print(e.animal.name)


# add function to variable
test = cat_murchik.say
test()
# we can use 'test' as parameter
# def foo(test):
#     pass

