from builtins import range
from datetime import datetime
import asyncio
from numpy import random


async def counter(x,index):
    res = x**0.5
    print("\n sqrt " + str(x) + ' = ' + str(res))
    print("index " + str(index) + '  |'+ str(datetime.now()))
    # await asyncio.sleep(1.0)
    return res


async def slow_routine():
    numbers = random.randint(100, size=10)

    index = 0
    for x in numbers:
        await counter(x,index)
        index+=1
    # await asyncio.gather(*futures)
    # return datetime.now()

#
# futures = []
# for x in range(3):
#     futures.append(slow_routine())

# [slow_routine() for x in range(3)]

print("\nSTART "+ str(datetime.now()))
loop = asyncio.get_event_loop()
loop.run_until_complete(slow_routine())
print("\nTHE END " + str(datetime.now()))
loop.close()
