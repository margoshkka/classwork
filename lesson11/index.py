from flask import Flask, jsonify, request
app = Flask(__name__)
import json


tasks =[
    {'id': 1, 'title':'Learn Python', 'description': 'Read python books, Write programs', 'done': False},
    {'id': 2, 'title':'Learn English', 'description': 'Visit speaking club', 'done': False}
]


@app.route("/todo/api/tasks",methods = ['GET'])
def api_tasks():
    return jsonify({'tasks': tasks})


@app.route("/todo/api/tasks/<int:task_id>", methods = ['GET'])
def api_task_get(task_id):
    return jsonify({'task ' + str(task_id): [x for x in tasks if x["id"] == task_id][0]})


@app.route("/todo/api/tasks/", methods = ['POST'])
def api_task_post():
    content = request.get_json()
    content["id"] = tasks[len(tasks)-1]["id"] + 1
    print(content)
    tasks.append(content)

    return tasks.index(content)+1


@app.route("/")
def hello():
    return "Hello World!"