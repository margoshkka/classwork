class Property:
    def __get__(self, instance, owner=None):
        if self.getter is None:
            raise AttributeError

        return self.getter(instance)

    def __set__(self, instance, value):
        if self.setter is None:
            raise AttributeError

        self.setter(instance, value)

    def __delete__(self, instance):
        if self.deleter is None:
            raise AttributeError

        self.deleter(instance)

    def __init__(self, getter, setter, deleter):
        self.getter = getter
        self.setter = setter
        self.deleter = deleter


