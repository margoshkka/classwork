from lesson5.define_propery import Property


class Bar:
    def getx(self):
        return self._x

    def setx(self, value):
        self._x = value**2

    def delx(self):
        del self._x

    foo = Property(getx, setx, delx)


def main():
    bar = Bar()

    bar.foo = 2
    print(bar.foo)

    del bar.foo
    bar.foo


if __name__ == '__main__':
    main()
