from collections import namedtuple


class Square(int):
    # it is always static method
    # it is instancing your class
    def __new__(cls, n, *args, **kwargs):
        return super().__new__(cls, n**2)


# returns class
Person = namedtuple("Person", ['name', 'age'])


class Person(Person):
    def __new__(cls, *args, **kwargs):
        kwargs['name'] = kwargs['name'].title()
        return super().__new__(cls,*args, **kwargs)


def main():
    sq = Square(12)
    print(sq)

    # p = Person(name='Vasya', age=20)
    # # you can use this ways:
    # # p.name
    # # p[0]
    # name, age = p
    # print('name: ' + name + '\nage: ' + str(age))

    p2 = Person(name="margo k", age=22)
    name, age = p2
    print('name: ' + name + '\nage: ' + str(age))


if __name__ == '__main__':
    main()
