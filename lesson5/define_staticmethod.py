class StaticMethod:
    def __init__(self, f):
        self.f = f

    def __get__(self, instance, owner=None):
        return self.f


class ClassMethod:
    def __init__(self, f):
        self.f = f

    def __get__(self, instance, owner=None):
        def wrapper(*arg,**kwargs):
            return self.f(owner, *arg, **kwargs)
        return wrapper


class BoundMethod:
    def __init__(self, f):
        self.f = f

    def __get__(self, instance, owner=None):
        def wrapper(*arg, **kwargs):
            return self.f(instance, *arg, **kwargs)

        return wrapper

