# Python3 basics
#fibbonaci


# def fibonacci_recursion(n, *list):
#     fibonachi_list = list
#     if n <= 0:
#         return fibonachi_list
#     elif n == 1 or n == 2:
#         fibonachi_list.append(1)
#         return [1, fibonachi_list]
#     res = fibonacci_recursion(n - 1) + fibonacci_recursion(n - 2)
#     fibonachi_list.append(res)
#     return [res, fibonachi_list]


# res = fibonacci_recursion(4, [])
# for i in res:
#     print(i)


def fib(n):
    if n <= 0:
        return
    yield 0
    yield 1
    a, b = 0
    for i in range(n):
        a, b = b, a+b
        yield b


for i in fib(10):
    print(i)

